from __future__ import division
from math import sqrt as sqrt
from itertools import product as product
from collections import namedtuple
import torch

SizeTuple = namedtuple('SizeTuple', 'w h')
OFFSET_X = 0.5
OFFSET_Y = 0.5


def get_step_size(im_dim, feature_dim):
    return round(im_dim/feature_dim)


class PriorBox(object):
    """Compute priorbox coordinates in center-offset form for each source
    feature map.
    """
    def __init__(self, cfg, cuda=None):
        super(PriorBox, self).__init__()
        self.to_device_in_first_fw = cuda if cuda is not None else False
        try:
            w = cfg['min_dim_w']
            h = cfg['min_dim_h']
        except KeyError:
            # old style
            w = h = cfg['min_dim']
        self.image_size = SizeTuple(w=w, h=h)
        # AJB - this comment is incorrect : number of priors for feature map location
        self.num_priors = len(cfg['aspect_ratios'])
        self.variance = cfg['variance'] or [0.1]
        self.feature_maps = cfg.get('feature_maps', None)
        self.min_sizes = cfg['min_sizes']
        self.max_sizes = cfg['max_sizes']
        self.steps = cfg.get('steps', None)
        self.aspect_ratios = cfg['aspect_ratios']
        self.clip = cfg.get('clip', False)
        # TODO: Redundant?
        # self.version = cfg['name']
        for v in self.variance:
            if v <= 0:
                raise ValueError('Variances must be greater than 0')

    def forward(self, override_feature_sizes=None):
        # print('now creating priors')
        if override_feature_sizes is not None:
            # hack for now - this must be a tuple
            self.feature_maps = override_feature_sizes
            self.steps = None
        boxes = []
        prior_count = 0
        for k, f in enumerate(self.feature_maps):
            feature_size_h, feature_size_w = f if isinstance(f, tuple) else (f, f)
            # print('creating priors for feature map %d with size (w=%d x h=%d)' % (k, feature_size_w, feature_size_h))
            if self.steps is None:
                step_size_w = get_step_size(self.image_size.w, feature_size_w)
                step_size_h = get_step_size(self.image_size.h, feature_size_h)
            else:
                step_size_w = self.steps[k]
                step_size_h = self.steps[k]
            # print('grid step (w=%d,h=%d)' % (step_size_w, step_size_h))
            for i in range(feature_size_h):
                for j in range(feature_size_w):
                    # unit center x,y
                    cx = (j + OFFSET_X) * float(step_size_w) / self.image_size.w
                    cy = (i + OFFSET_Y) * float(step_size_h) / self.image_size.h
                    # aspect_ratio: 1
                    # rel size: min_size
                    min_size = self.min_sizes[k]
                    s_k = SizeTuple(w=min_size/self.image_size.w, h=min_size/self.image_size.h)
                    # print('doing grid point ( %d , %d ) : ( %f, %f, %f, %f )' % (i, j, cx, cy, s_k.w, s_k.h))
                    # msg_tuple = (i, j, cx * self.image_size.w, cy * self.image_size.h,
                    #              s_k.w * self.image_size.w, s_k.h * self.image_size.h)
                    # print('doing grid point ( %d , %d ) : ( %f, %f, %f, %f )' % msg_tuple)
                    boxes += [cx, cy, s_k.w, s_k.h]
                    prior_count += 1

                    # aspect_ratio: 1
                    # rel size: sqrt(s_k * s_(k+1))
                    s_k_prime_w = sqrt(s_k.w * (self.max_sizes[k]/self.image_size.w))
                    s_k_prime_h = sqrt(s_k.h * (self.max_sizes[k]/self.image_size.h))
                    boxes += [cx, cy, s_k_prime_w, s_k_prime_h]
                    prior_count += 1

                    # rest of aspect ratios
                    for ar in self.aspect_ratios[k]:
                        boxes += [cx, cy, s_k.w*sqrt(ar), s_k.h/sqrt(ar)]
                        prior_count += 1
                        boxes += [cx, cy, s_k.w/sqrt(ar), s_k.h*sqrt(ar)]
                        prior_count += 1
            # print('prior count %d ' % prior_count)
        # back to torch land
        output = torch.Tensor(boxes).view(-1, 4)
        if self.clip:
            output.clamp_(max=1, min=0)
        return output
