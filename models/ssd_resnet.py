import torch.nn as nn
from torchvision.models.resnet import Bottleneck, BasicBlock
from models.ssd_vgg import SSD, extras, mbox, add_extras, add_extras_512


# This function is derived from torchvision ResNet
# https://github.com/pytorch/vision/blob/master/torchvision/models/resnet.py
def resnet(layers_definition, block=BasicBlock):
    inplanes = 64

    def make_layer(inplanes, block, planes, blocks, stride=1):
        downsample = None
        if stride != 1 or inplanes != planes * block.expansion:
            downsample = nn.Sequential(nn.Conv2d(inplanes, planes * block.expansion, kernel_size=1, stride=stride, bias=False),
                          nn.BatchNorm2d(planes * block.expansion))

        layers = []
        layers.append(block(inplanes, planes, stride, downsample))
        inplanes = planes * block.expansion
        for i in range(1, blocks):
            layers.append(block(inplanes, planes))

        return layers

    layers = []
    layers += [nn.Conv2d(3, 64, kernel_size=7, stride=2, padding=3, bias=False)]
    layers += [nn.BatchNorm2d(64)]
    layers += [nn.ReLU(inplace=True)]
    layers += [nn.MaxPool2d(kernel_size=3, stride=2, padding=1)]
    layers += [*make_layer(inplanes, block, 64, layers_definition[0])]
    inplanes = 64 * block.expansion
    layers += [*make_layer(inplanes, block, 128, layers_definition[1], stride=2)]
    inplanes = 128 * block.expansion
    layers += [*make_layer(inplanes, block, 256, layers_definition[2], stride=2)]
    inplanes = 256 * block.expansion
    layers += [*make_layer(inplanes, block, 512, layers_definition[3], stride=2)]
    layers += [nn.AvgPool2d(7, stride=1)]

    return layers


def multibox_resnet(features, extra_layers, cfg, num_classes, features_source):
    loc_layers = []
    conf_layers = []
    for k, v in enumerate(features_source):
        loc_layers += [nn.Conv2d(features[v].conv2.out_channels, # for resnet50 and resnet101 nn.Conv2d(features[v].conv3.out_channels
                                 cfg[k] * 4, kernel_size=3, padding=1)]
        conf_layers += [nn.Conv2d(features[v].conv2.out_channels,
                                  cfg[k] * num_classes, kernel_size=3, padding=1)]
    for k, v in enumerate(extra_layers[1::2], 2):
        loc_layers += [nn.Conv2d(v.out_channels, cfg[k]
                                 * 4, kernel_size=3, padding=1)]
        conf_layers += [nn.Conv2d(v.out_channels, cfg[k]
                                  * num_classes, kernel_size=3, padding=1)]
    return features, extra_layers, (loc_layers, conf_layers)


def build_ssd_with_resnet(cfg, phase, variant=300, size=(300, 300), num_classes=21, extras_in_channels=512,
                          resnet_type='resnet50', dropout=0.1):
    if phase != "test" and phase != "train":
        print("ERROR: Phase: " + phase + " not recognized")
        return

    if resnet_type == 'resnet50':
        block = Bottleneck
        layers_definition = [3, 4, 6, 3]
        features_source = [16, -2]
        conv4_3 = 16
        l2_n_channels = 1024
    elif resnet_type == 'resnet101':
        block = Bottleneck
        layers_definition = [3, 4, 6, 3]
        features_source = [16, -2]
        conv4_3 = 16
        l2_n_channels = 1024
    else:
        raise ValueError("resnet type: " + resnet_type + " not recognized. We support only 'resnet50' or 'resnet101'")

    add_extras_func = {'res': add_extras, 300: add_extras, 512: add_extras_512}[variant]
    base_, extras_, head_ = multibox_resnet(resnet(layers_definition=layers_definition, block=block),
                                            add_extras_func(extras[str(variant)], extras_in_channels),
                                            mbox[str(variant)], num_classes, features_source=features_source)
    return SSD(phase, size, base_, extras_, head_, num_classes=num_classes, cfg=cfg, conv4_3=conv4_3, dropout=dropout,
               l2_n_channels=l2_n_channels, add_conv43_pool=nn.MaxPool2d(kernel_size=3, stride=1, padding=1))
