import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.autograd import Variable
from layers import *
import os
from common.loading import ModuleWithForgivingLoad

base = {
    'vgg16': [64, 64, 'M', 128, 128, 'M', 256, 256, 256, 'C', 512, 512, 512, 'M',
              512, 512, 512],
    'vgg19': [64, 64, 'M', 128, 128, 'M', 256, 256, 256, 256, 'M', 512, 512, 512, 512, 'M',
              512, 512, 512, 512],
}
extras = {
    '300': [256, 'S', 512, 128, 'S', 256, 128, 'S', 256, 128, 'S', 256],
    '512': [256, 'S', 512, 128, 'S', 256, 128, 'S', 256, 128, 'S', 256, 128, 256],
}
mbox = {
    # number of priors per feature map location, per scale
    # The original implementation has 4 priors in the first and last 2 scales
    # and six in the rest.
    '300': [4, 6, 6, 6, 4, 4],
    # in 512 there is an extra scale.
    '512': [4, 6, 6, 6, 6, 4, 4],
}


class SSD(ModuleWithForgivingLoad):
    """Single Shot Multibox Architecture
    The network is composed of a base VGG network followed by the
    added multibox conv layers.  Each multibox layer branches into
        1) conv2d for class conf scores
        2) conv2d for localization predictions
        3) associated priorbox layer to produce default bounding
           boxes specific to the layer's feature map size.
    See: https://arxiv.org/pdf/1512.02325.pdf for more details.

    Args:
        phase: (string) Can be "test" or "train"
        size: input image size
        base: VGG16 layers for input, size of either 300 or 500
        extras: extra layers that feed to multibox loc and conf layers
        head: "multibox head" consists of loc and conf conv layers
    """

    def __init__(self, phase, size, base, extras, head, num_classes, cfg, conv4_3, dropout=0.1, l2_n_channels=512,
                 add_conv43_pool=None):
        super(SSD, self).__init__()
        self.phase = phase
        self.num_classes = num_classes
        self.cfg = cfg
        self.size = size if isinstance(size, tuple) else (size, size)
        self.conv4_3 = conv4_3
        self.dropout_val = dropout

        # SSD network
        self.features = nn.ModuleList(base)
        self.add_layer = add_conv43_pool
        # Layer learns to scale the l2 normalized features from conv4_3
        self.L2Norm = L2Norm(l2_n_channels, 20)
        self.dropout = nn.Dropout2d(p=self.dropout_val, inplace=False)
        self.extras = nn.ModuleList(extras)

        self.loc = nn.ModuleList(head[0])
        self.conf = nn.ModuleList(head[1])

        # create priors:
        # change cfg so that feature_maps hold the actual size of the feature maps
        self.priorbox = PriorBox(self.cfg)
        self.verify_priors_match_output = False
        # self.priors = Variable(self.priorbox.forward(), volatile=True)
        self.priors = Variable(torch.Tensor([]), volatile=True)
        self.softmax = nn.Softmax(dim=-1)
        self.detect = Detect(num_classes=num_classes, bkg_label=cfg.get('background_label', 0),
                             top_k=cfg.get('top_k', 200),
                             conf_thresh=cfg.get('conf_thresh', 0.01),
                             nms_thresh=cfg.get('nms_thresh', 0.45),
                             cfg=cfg)

    def forward(self, x):
        """Applies network layers and ops on input image(s) x.

        Args:
            x: input image or batch of images. Shape: [batch,3,300,300].

        Return:
            Depending on phase:
            test_orig:
                Variable(tensor) of output class label predictions,
                confidence score, and corresponding location predictions for
                each object detected. Shape: [batch,topk,7]

            train_orig:
                list of concat outputs from:
                    1: confidence layers, Shape: [batch*num_priors,num_classes]
                    2: localization layers, Shape: [batch,num_priors*4]
                    3: priorbox layers, Shape: [2,num_priors*4]
        """
        sources = list()
        loc = list()
        conf = list()

        # apply vgg up to conv4_3 relu
        for k in range(self.conv4_3):
            x = self.features[k](x)

        if self.add_layer is not None:
            x = self.add_layer(x)

        s = self.L2Norm(x)
        sources.append(s)

        # apply vgg up to fc7
        for k in range(self.conv4_3, len(self.features)):
            x = self.features[k](x)
        sources.append(x)

        # apply dropout
        if self.training and self.dropout_val > 0:
            x = self.dropout(x)

        # apply extra layers and cache source layer outputs
        for k, v in enumerate(self.extras):
            x = F.relu(v(x), inplace=True)
            if k % 2 == 1:
                sources.append(x)

        """
        now we know the feature sizes, time to create the priors 
        """
        if self.priors.shape == torch.Size([0]):
            current_features = [tuple(source.shape[-2:]) for source in sources]
            self.priors = Variable(self.priorbox.forward(override_feature_sizes=current_features), volatile=True)
            self.verify_priors_match_output = True
            if torch.cuda.is_available():
                device = torch.cuda.current_device()
                self.priors.cuda(device=device)

            # need to push priors to cuda now?

        # apply multibox head to source layers
        for (x, l, c) in zip(sources, self.loc, self.conf):
            loc.append(l(x).permute(0, 2, 3, 1).contiguous())
            conf.append(c(x).permute(0, 2, 3, 1).contiguous())

        loc = torch.cat([o.view(o.size(0), -1) for o in loc], 1)
        conf = torch.cat([o.view(o.size(0), -1) for o in conf], 1)
        # first time - test that number of priors matches loc and conf
        if self.verify_priors_match_output:
            assert int(loc.shape[-1])/4 == int(self.priors.shape[0]), "Bug!"
            assert int(conf.shape[-1])/self.num_classes == int(self.priors.shape[0]), "Bug!"
            self.verify_priors_match_output = False

        if not self.training:
            output = self.detect(
                loc.view(loc.size(0), -1, 4),                   # loc preds
                self.softmax(conf.view(conf.size(0), -1,
                             self.num_classes)),                # conf preds
                self.priors.type(type(x.data))                  # default boxes
            ).clamp(0., 1.)
        else:
            output = (
                loc.view(loc.size(0), -1, 4),
                conf.view(conf.size(0), -1, self.num_classes),
                self.priors.view(1, -1, 4)
            )
        return output

    def load_weights(self, base_file):
        other, ext = os.path.splitext(base_file)
        if ext == '.pkl' or '.pth':
            print('Loading weights into state dict...')
            self.load_state_dict(torch.load(base_file,
                                 map_location=lambda storage, loc: storage))
            print('Finished loading weights')
        else:
            print('Sorry only .pth and .pkl files supported.')


# This function is derived from torchvision VGG make_layers()
# https://github.com/pytorch/vision/blob/master/torchvision/models/vgg.py
def vgg(cfg, i, batch_norm=False, conv7_in_dim=1024):
    layers = []
    in_channels = i
    for v in cfg:
        if v == 'M':
            layers += [nn.MaxPool2d(kernel_size=2, stride=2)]
        elif v == 'C':
            layers += [nn.MaxPool2d(kernel_size=2, stride=2, ceil_mode=True)]
        else:
            conv2d = nn.Conv2d(in_channels, v, kernel_size=3, padding=1)
            if batch_norm:
                layers += [conv2d, nn.BatchNorm2d(v), nn.ReLU(inplace=True)]
            else:
                layers += [conv2d, nn.ReLU(inplace=True)]
            in_channels = v
    pool5 = nn.MaxPool2d(kernel_size=3, stride=1, padding=1)
    conv6 = nn.Conv2d(512, 1024, kernel_size=3, padding=6, dilation=6)
    layers += [pool5, conv6,
               nn.ReLU(inplace=True)]
    if conv7_in_dim > 0:
        conv7 = nn.Conv2d(conv7_in_dim, 1024, kernel_size=1)
        layers += [conv7, nn.ReLU(inplace=True)]
    return layers


def add_extras(cfg, i, batch_norm=False):
    # Extra layers added to VGG for feature scaling
    layers = []
    in_channels = i
    flag = False
    for k, v in enumerate(cfg):
        if in_channels != 'S':
            if v == 'S':
                layers += [nn.Conv2d(in_channels, cfg[k + 1],
                           kernel_size=(1, 3)[flag], stride=2, padding=1)]
            else:
                layers += [nn.Conv2d(in_channels, v, kernel_size=(1, 3)[flag])]
            flag = not flag
        in_channels = v
    return layers


def add_extras_512(cfg, i, hmap_channels=0):
    # Extra layers added to VGG for feature scaling
    layers = []
    add_hmap_chans = 1
    in_channels = i
    flag = False
    for k, v in enumerate(cfg[:-1]):
        if in_channels != 'S':
            if v == 'S':
                layers += [nn.Conv2d(in_channels, cfg[k + 1], kernel_size=(1, 3)[flag], stride=2, padding=1)]
            else:
                layers += [nn.Conv2d(in_channels + add_hmap_chans*hmap_channels, v, kernel_size=(1, 3)[flag])]
            flag = not flag
            add_hmap_chans = 0
        else:
            add_hmap_chans = 1
        in_channels = v

    layers += [nn.Conv2d(cfg[-2], cfg[-1], kernel_size=4, padding=1)]
    return layers


def multibox(vgg, extra_layers, cfg, num_classes, vgg_source):
    loc_layers = []
    conf_layers = []
    for k, v in enumerate(vgg_source):
        loc_layers += [nn.Conv2d(vgg[v].out_channels,
                                 cfg[k] * 4, kernel_size=3, padding=1)]
        conf_layers += [nn.Conv2d(vgg[v].out_channels,
                        cfg[k] * num_classes, kernel_size=3, padding=1)]
    for k, v in enumerate(extra_layers[1::2], 2):
        loc_layers += [nn.Conv2d(v.out_channels, cfg[k]
                                 * 4, kernel_size=3, padding=1)]
        conf_layers += [nn.Conv2d(v.out_channels, cfg[k]
                                  * num_classes, kernel_size=3, padding=1)]
    return vgg, extra_layers, (loc_layers, conf_layers)


def build_ssd_with_vgg(cfg, phase, variant=300, size=(300, 300), num_classes=21, vgg_type='vgg16', dropout=0.1):
    if phase != "test" and phase != "train":
        print("ERROR: Phase: " + phase + " not recognized")
        return
    if vgg_type == "vgg16":
        vgg_source = [21, -2]
        conv4_3 = 23
    elif vgg_type == "vgg19":
        vgg_source = [25, -2]
        conv4_3 = 27
    else:
        raise ValueError("vgg_type: " + vgg_type + " not recognized. We support only 'vgg16' or 'vgg19'")

    add_extras_func = {300: add_extras, 512: add_extras_512}[variant]
    base_, extras_, head_ = multibox(vgg(base[str(vgg_type)], 3),
                                     add_extras_func(extras[str(variant)], 1024),
                                     mbox[str(variant)], num_classes, vgg_source=vgg_source)
    return SSD(phase, size, base_, extras_, head_, num_classes=num_classes, cfg=cfg, conv4_3=conv4_3, dropout=dropout)
