from logging import ERROR

import cv2
import datetime
from argparse import ArgumentParser

import yaml
import numpy as np
import torch
from torch.autograd import Variable

from allegroai import DatasetVersion, DataView, Task, InputModel, DataPipe
from allegroai.dataview import FilterByRoi
from allegroai.dataview import IterationOrder
from allegroai.debugging.timer import Timer
from allegroai_api.services import tasks

from common.utils import ssd_output_to_allegro_format, setup_pytorch03, ImageSizeTuple, build_net, make_deterministic, \
    resolve_resize_strategy

# Please see __main__ below.
# =================  GLOBAL NAMES - meant to be overridden by ui, help reuse task id  ==============================
TASK_NAME = 'Create annotated version using SSD'
PROJECT_NAME = 'pytorch ssd'


# convert ROI to polygon (as supported by the frame)
def box_to_poly(box):
    top_left = [box[0], box[1]]
    top_right = [box[2], box[1]]
    bottom_right = [box[2], box[3]]
    bottom_left = [box[0], box[3]]
    poly = np.hstack((top_left, top_right, bottom_right, bottom_left)).tolist()
    return poly


# uses model design values, for which there is an override using execution parameters
def ssd_inference_on_image(single_image):
    single_image = torch.from_numpy(single_image)
    in_image = Variable(single_image.cuda())

    # forward
    detections = net.data_parallel(in_image)
    detections = detections.data.cpu().numpy()
    all_image_boxes, all_image_scores = \
        ssd_output_to_allegro_format(numpy_ssd_output=detections,
                                     conf_thresh=config_params['test_detection_conf_thresh'],
                                     img_width=config_params['min_dim_w'],
                                     img_height=config_params['min_dim_h'])
    return all_image_boxes, all_image_scores


frame_process_timer = Timer()
frame_get_image_timer = Timer()
frame_inference_timer = Timer()


if __name__ == '__main__':
    parser = ArgumentParser(description='Create annotated dataset version')
    parser.add_argument('--new-version-name', help='Name for the generated version')
    parser.add_argument('--feature-extraction-type', default='vgg16', type=str, help='Feature extraction network')
    parser.add_argument('--debug-image-freq', default=100, type=int, help='upload debug images every n frames')
    parser.add_argument('--create-at-conf-thresh', default=0, type=float, help='threshold used for inference')
    parser.add_argument('--override-input-w', default=0, type=int, help='override specified w')
    parser.add_argument('--override-input-h', default=0, type=int, help='override specified h')
    parser.add_argument('--num-workers', default=32, type=int, help='Number of workers used in dataloading')
    parser.add_argument('--no-create-version', action="store_true", help='run, but do not create a new version')
    parser.add_argument('--upload-destination', default='s3://allegro-examples', type=str,
                        help='Destination to upload debug images and models')

    # new additions, pending refactoring
    # resize strategy
    parser.add_argument('--resize-strategy', type=str, default='bigger_keep_ar',
                        help='In case of several sources per frame, only choose this source id to train on')

    # source id
    parser.add_argument('--source-id', type=str, default=None,
                        help='In case of several sources per frame, only choose this source id to annotate on')

    # ========= (1) Define task ==============================
    task = Task.current_task(default_project_name=PROJECT_NAME, default_task_name=TASK_NAME,
                             default_task_type=tasks.TaskTypeEnum.testing)
    seed = task.get_random_seed()
    make_deterministic(seed)  # setup random seed from task for reproducibility

    #################################
    # Connect arguments to the task #
    #################################
    task.connect(parser)
    args = parser.parse_args()
    if args.source_id == '':
        args.source_id = None

    logger = task.get_logger()
    logger.set_default_upload_destination(uri=args.upload_destination)
    logger.console('Running arguments: %s' % str(args))

    # ========= (2) Define input model, its labels and config  ==============================
    INPUT_MODEL_URL = 'https://s3.amazonaws.com/allegro-models/Pytorch_SSD_Person_Detector.pth'
    INPUT_MODEL_NAME = 'Pytorch SSD person detector'
    input_model = InputModel.import_model(weights_url=INPUT_MODEL_URL, name=INPUT_MODEL_NAME,
                                          design=None, label_enumeration=None)
    task.connect(input_model)
    # either input model or task itself must have a design
    net_design = task.get_model_design() or input_model.design
    if not net_design or len(net_design) == 0:
        raise IOError('A network design must be present in order to load model. got %s' % net_design)
    config_params = yaml.load(net_design)

    # either input model or task itself must have labels
    model_labels = task.get_labels_enumeration() or input_model.labels
    if not model_labels or all([value <= 0 for value in model_labels.values()]):
        default_labels = {'background': 0,  'person': 1}
        logger.console('No input model labels, or no positive ids : {} ,'
                       'using default labels for this script'.format(model_labels), level=ERROR)
        model_labels = default_labels

    if args.create_at_conf_thresh > 0:
        previous_conf = config_params.pop('test_detection_conf_thresh', 'Not Specified')
        config_params['test_detection_conf_thresh'] = float(args.create_at_conf_thresh)
        logger.console('Override: confidence thresh for inference is now %.2f instead of %s' %
                       (config_params['test_detection_conf_thresh'], str(previous_conf)))
    if args.override_input_w > 0:
        previous_w = config_params['min_dim_w']
        config_params['min_dim_w'] = args.override_input_w
        logger.console('Override: input width is now %d instead of %d' %
                       (config_params['min_dim_w'], previous_w))
    if args.override_input_h > 0:
        previous_h = config_params['min_dim_h']
        config_params['min_dim_h'] = args.override_input_h
        logger.console('Override: input width is now %d instead of %d' %
                       (config_params['min_dim_h'], previous_h))

    # ========= (3) Define dataview  ==============================
    # Note: you cannot annotate a public dataset, so this task requires that you add your own dataset and version
    dataview = DataView(iteration_order=IterationOrder.sequential, iteration_infinite=False)
    DATASET_NAME = 'Tutorial'  # Please replace with a non public dataset
    VERSION_NAME = 'Data registration'  # Please replace with a non public version
    try:
        dataview.add_query(dataset_name=DATASET_NAME, version_name=VERSION_NAME,
                           filter_by_roi=FilterByRoi.disabled)
    except Exception as ex:
        logger.console(str(ex), ERROR)
    task.connect(dataview)

    # update labels here if needed, these are taken from the input model labels:
    id_to_model_label = {v: k for k, v in model_labels.items()}

    # ========= (4) Validate input ==============================
    # Check that dataset and version details are valid...
    all_versions = dataview._dataview.data.versions
    if len(all_versions) != 1:
        num_queries = len(all_versions)
        raise ValueError('This task is designed for a single dataset query only in order to create a version'
                         ', received {} different versions/datasets'.format(num_queries))

    orig_version = DatasetVersion.get_version(
        dataset_id=all_versions[0].dataset,
        version_id=all_versions[0].version
    )
    logger.console('Found version: %s' % orig_version.version_name)

    # ========= (5) Create new target version ==============================
    if args.no_create_version:
        raise NotImplemented('Inference on a single version without creation of a new one is still not available')

    if not args.new_version_name:
        args.new_version_name = ' | '.join([orig_version.version_name, task.name + ' - ' +
                                            str(datetime.datetime.now().strftime("%Y-%m-%d %H:%M"))])
    logger.console('Creating new dataset version: %s' % args.new_version_name)
    try:
        target_version = DatasetVersion.create_version(
            name=args.new_version_name,
            dataset_id=orig_version.dataset_id,
            parent_version_ids=[orig_version.version_id]
        )
    except Exception as ex:
        from logging import ERROR
        msg = 'Dataset version creation failed. Are you sure you are able to edit this dataset?'
        logger.console(str(ex), level=ERROR, exc_info=True)
        logger.console(msg, level=ERROR)
        logger.flush()
        raise PermissionError(msg)
    # ========= (6) Build the network ==============================
    #
    target_image_size = ImageSizeTuple(w=config_params['min_dim_w'], h=config_params['min_dim_h'])
    cuda_is_on = setup_pytorch03(args=None, logger=logger)
    net = build_net(cfg=config_params, training=False,
                    args=args, input_image_size=target_image_size, use_cudnn=cuda_is_on)
    weights_file = input_model.get_weights()
    logger.console('Loading net weights from {}...'.format(weights_file))
    net.ssd.forgiving_load(weights_file, logger=logger)

    orig_iterator = dataview.get_iterator()

    if args.source_id:
        source_id = [args.source_id]
    else:
        source_id = None
    resize_strategy = resolve_resize_strategy(args, logger)
    pipe = DataPipe(orig_iterator, num_workers=args.num_workers,
                    frame_cls_kwargs=dict(target_width_height=target_image_size,
                                          resize_strategy=resize_strategy,
                                          default_source_ids_to_load=source_id))

    # ========= (7) Fill each frame with prediction results ==============================
    #
    iterator = pipe.get_iterator()

    with target_version.get_bulk_context() as edit:
        for i, image_frame in enumerate(iterator):
            frame_process_timer.tic()
            frame_get_image_timer.tic()
            meta_frame = image_frame.get_meta_data()
            all_sources = image_frame.get_sources()
            if args.source_id:
                sources = [args.source_id]
            else:
                sources = [all_sources[0]]
            prev_rois = []
            if len(all_sources) > 1:
                for src in all_sources:
                    if src not in sources:
                        curr_rois = [roi for roi in meta_frame.rois if src in roi.sources]
                        prev_rois = prev_rois + curr_rois
            img_rois = []
            for source in sources:
                im = image_frame.get_data(source_id=source).astype(np.float32)
                image = im.transpose(2, 0, 1)
                img_tensor = np.zeros((1, 3) + tuple(target_image_size)[::-1], dtype=np.float32)
                d1, d2, d3 = image.shape
                img_tensor[0, :d1, :d2, :d3] = image
                frame_get_image_timer.toc()

                frame_inference_timer.tic()
                boxes, scores = ssd_inference_on_image(img_tensor)
                frame_inference_timer.toc()

                resize_box_factors = [meta_frame.width / d3, meta_frame.height / d2] * 2
                if boxes is not None:
                    for n, bbox in enumerate(boxes):
                        # note that this works since we have a single image in the batch
                        box = bbox[1:5]
                        label = [id_to_model_label.get(bbox[5])]
                        score = scores[n]
                        img_rois.append(DataView.FrameRoi(
                            sources=[source],
                            label=label,
                            poly=box_to_poly(list(box*resize_box_factors)),
                            confidence=score))

                if i > 0 and i % args.debug_image_freq == 0:
                    img = im.copy()
                    # TODO use debug images here instead of this
                    if boxes is not None:
                        boxes = boxes
                        for n, b in enumerate(boxes):
                            ann = [id_to_model_label.get(b[5])]
                            C = (0, 0, 0)
                            b = np.array(b).astype('int')
                            cv2.rectangle(img, pt1=tuple(b[1:3]), pt2=tuple(b[3:5]), color=C, thickness=2)
                    logger.report_image_and_upload(title='Inference result', series='img_%d' % i, iteration=i,
                                                   matrix=img.astype(np.uint8))
                    logger.flush()
            meta_frame.rois = prev_rois + img_rois
            if not meta_frame.meta:
                meta_frame.meta = {}
            meta_frame.meta['auto_annotate'] = '1'
            # from now on this frame can be sent
            edit.update_frame(meta_frame)
            frame_process_timer.toc()
            if i > 0 and i % 10 == 0:
                t_process = frame_process_timer.average_time
                t_get_image = frame_get_image_timer.average_time
                t_inference = frame_inference_timer.average_time
                logger.console(
                    'Currently at frame %d, t_frame: %.3f (sec) t_inference: %.3f (sec) t_overhead: %.3f (sec)' %
                    (i, t_process, t_inference, t_process - t_get_image - t_inference))

        logger.console('Sending frames')

    # (7.1) Commit
    logger.console('Committing new dataset version: %s' % args.new_version_name)
    commit_result = target_version.commit_version()
    logger.console('Commit result %s ' % commit_result.response)

    # (7.2) Publish
    target_version.publish_version()
    logger.console('New dataset version published')
    logger.console('Done')
